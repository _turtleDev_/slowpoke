import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent }  from './app.component';
import { NewTaskComponent } from './new-task.component';
import { TaskCalendarComponent } from './task-calender.component';

@NgModule({
  imports: [ 
    BrowserModule,
    FormsModule 
  ],
  declarations: [ 
    AppComponent,
    NewTaskComponent,
    TaskCalendarComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
