import { Component } from '@angular/core';
import { Task } from './task';

@Component({
    selector: 'new-task',
    template: `
        <div class="new-task">
          <button class="new-task-button" (click)="newTask()">Add Task</button>
          <div class="new-task-inner" *ngIf="task">
            <h2>Create new task</h2>
            <div>
              <input [(ngModel)]="task.title" type=text placeholder="name"/>
            </div>
            <div>
              <textarea [(ngModel)]="task.description" placeholder="description"></textarea>
            </div>
            <div>
              <input [(ngModel)]="task.dueDate" type="date"/>
            </div>
            <div>
              <button (click)="saveTask()">Save</button>
              <button (click)="cancelTask()">Cancel</button>
            </div>
          </div>
        </div>
    `
})
export class NewTaskComponent {
    task: Task = null;

    newTask() {
        if ( this.task ) 
            return;
        this.task = new Task();
    }

    saveTask() {
        if ( !this.task )
            return;
        
        this.task.save();
        this.reset();
    }
    
    cancelTask() {
        this.reset();
    }

    reset() {
        this.task = null;
    }

}