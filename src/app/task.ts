import { Moment } from 'moment';
/**
 * side note: Task has a few methods around registering and 
 * dispatching events. It would probably be a good idea
 * to create an event emitter based service to replace this.
 */
let store: Object = {}
let observers: Function[] = [];
let lastId = 0;

/**
 * for debugging
 */
(<any>window).store = store;

export class Task {

    id: number;
    title: string;
    description: string;
    private _dueDate: string;
    done: Boolean;

    get dueDate() {
        return this._dueDate;
    }

    set dueDate(val) {
        this._dueDate = val;
        Task.dispatchUpdate();
    }

    constructor() {
        this.id = ++lastId;
        this.done = false;
    }

    save(): void {
        store[this.id] = this;
        Task.dispatchUpdate();
    }

    static dispatchUpdate() {
        observers.forEach(cb => cb(this))
    }

    static subscribe(cb:Function) {
        observers.push(cb);
    }

    static getTasks(): Array<Task> {
        return Object.getOwnPropertyNames(store)
            .map(k => store[k]);
    }

    static getTaskSet() {
        let dateMap = {};
        this.getTasks()
        .map((task) => {
            if ( dateMap[task.dueDate] )
                dateMap[task.dueDate].push(task);
            else
                dateMap[task.dueDate] = [task];
        })
        return dateMap;
    }
}   