import { Component, OnInit } from '@angular/core';
import { Task } from './task';

@Component({
    selector: 'task-calendar',
    template: `
        <h2>Task Calendar</h2>
        <p>{{monthString}}</p>
        <ul class="calendar-container">
            <li class="calendar-day" *ngFor="let day of dayData">
                <div>
                    {{day.date}}
                </div>
                <div *ngIf="day.tasks.length">
                    <div *ngFor="let task of day.tasks">
                        <div [class.done]="task.done" class="calendar-task" (click)="edit(task)">
                            {{task.title}}
                        </div>
                    </div>
                </div>
            </li>
            <div *ngIf="editorActive">
                <div class="calendar-editor">
                    <h2>editing "{{editorTarget.title}}"</h2>
                    <input [(ngModel)]="editorTarget.title" type="text" placeholder="title"/>
                    <textarea [(ngModel)]="editorTarget.description" placeholder="description"></textarea>
                    <input [(ngModel)]="editorTarget.dueDate" type="date"/>
                    <div>
                     done:
                         <input [(ngModel)]="editorTarget.done" type="checkbox">
                    </div>
                    <button (click)="save()">save</button>
                    <button (click)="cancel()">cancel</button>
                </div>
            </div>
        </ul>
    `
})
export class TaskCalendarComponent implements OnInit {

    totalDays: number = 31;
    year: number = 2017;
    month: number = 5;
    monthString: string = "May";
    dayData: Array<any> = [];
    editorActive = false;
    editorTarget: Task = null;

    edit(task: Task) {
        this.editorActive = true;
        this.editorTarget = task;
    }

    compute() {

        let days: any = [];
        let taskSet = Task.getTaskSet();

        for ( let i = 1; i <= this.totalDays; ++i) {

            const dayStr = new Date(
                Date.UTC(this.year, this.month-1, i)
            ).toISOString().slice(0, 10);

            let day = {
                date: i,
                tasks: <any>null
            }

            if ( dayStr in taskSet ) 
                day.tasks = taskSet[dayStr];
            else
                day.tasks = [];
            
            days.push(day);
        }

        this.dayData = days;
    }

    ngOnInit() {
        this.compute()
        Task.subscribe(() => this.compute())
    }

    save() {
        this.editorTarget.save();
        this.editorTarget = null;
        this.editorActive = false;
    }
    
    cancel() {
        this.editorActive = false;
        this.editorTarget = null;
    }
}
