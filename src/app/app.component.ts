import { Component } from '@angular/core';

@Component({
  selector: 'app',
  template: `
    <h1>Taskr</h1>
    <new-task></new-task>
    <task-calendar></task-calendar>
  `,
})
export class AppComponent  { }
